<?php
/**
 * @package  Loggfy
 * @category Integration
 */

class Loggfy_Integration extends WC_Integration
{
    private $woo = false;
    private $status = false;
    private $session_id;

    /**
     * Init and hook in the integration.
     */
    public function __construct()
    {
        global $woocommerce;
        $this->woo = function_exists('WC') ? WC() : $woocommerce;
        $this->id = 'loggfy-connect';
        $this->method_title = __('Loggfy Integration Settings', 'loggfy-connect');
        $this->method_description = '';
        // Load the settings.
        $this->init_form_fields();
        $this->init_settings();
        $this->status = $this->get_option('status', false);
        // Actions.
        add_action('init', [$this, 'loggfy_manage_session']);
        add_action('woocommerce_update_options_integration_' . $this->id, array($this, 'process_admin_options'));
        add_action('woocommerce_init', array($this, 'loggfy_initiate_app'));
    }

    /**
     * Initialize integration settings form fields.
     */
    public function init_form_fields()
    {
        $this->form_fields = array(
            'status' => array(
                'title' => __('Activate Loggfy', 'loggfy'),
                'type' => 'checkbox',
                'label' => __('Activate Loggfy', 'loggfy'),
                'default' => 'no',
                'description' => ''
            ),
        );
    }

    public function loggfy_initiate_app()
    {
        if ($this->status === 'yes') {
            $this->loggfy_client_identify();
            add_filter('wp_head', array($this, 'loggfy_track_shop'));
            add_action('woocommerce_add_to_cart', array($this, 'loggfy_add_to_cart'), 10, 6);
            add_action('woocommerce_before_cart_item_quantity_zero', array($this, 'loggfy_remove_from_cart'), 10);
            add_action('woocommerce_remove_cart_item', array($this, 'loggfy_remove_from_cart'), 10);
            add_action('woocommerce_checkout_order_processed', array($this, 'loggfy_save_order'), 10);
        }
    }

    public function loggfy_add_to_cart($cart_item_key, $product_id, $quantity, $variation_id = false, $variation = false, $cart_item_data = false)
    {
        $product = $this->loggfy_get_product($product_id);
        $this->add_to_queue('add_to_cart', $this->product_data($product));
    }

    public function remove_from_cart($key_id)
    {
        if (!is_object($this->woo->cart)) {
            return true;
        }
        $cart_items = $this->woo->cart->get_cart();
        $removed_cart_item = isset($cart_items[$key_id]) ? $cart_items[$key_id] : false;
        if ($removed_cart_item) {
            $product = $this->loggfy_get_product($removed_cart_item['product_id']);
            $this->add_to_queue('remove_from_cart', $this->product_data($product));
        }
    }

    public function loggfy_track_shop()
    {
        if (is_product()) {
            $product = $this->loggfy_get_product(get_queried_object_id());
            $this->add_to_queue('product_view', $this->product_data($product));
        }

        if (is_product_category()) {
            $this->add_to_queue('category_view', 0, $this->loggfy_category_data(get_queried_object()));
        }

        if (is_cart()) {
            $this->add_to_queue('cart_view', []);
        }

        if (is_order_received_page()) {
            $this->add_to_queue('page_view', ['page' => 'Order Received']);

        } elseif (function_exists('is_checkout_pay_page') && is_checkout_pay_page()) {
            $this->add_to_queue('checkout_payment', ['page' => 'Checkout Page']);

        } elseif (is_checkout()) {
            $this->add_to_queue('checkout_start', ['page' => 'Checkout Started']);
        }

    }

    public function loggfy_category_data($category)
    {
        $data = array(
            'id' => $category->term_id,
            'name' => $category->name
        );
        return $data;
    }

    public function loggfy_get_product($product_id)
    {
        if (function_exists('wc_get_product')) {
            return wc_get_product($product_id);
        } else {
            return get_product($product_id);
        }
    }

    public function loggfy_manage_session()
    {
        if (\Loggfy\Libraries\Cookie::exists('SID')) {
            $this->session_id = \Loggfy\Libraries\Cookie::get('SID');
        } else {

            $token = md5(uniqid(mt_rand(), true));
            (new \Loggfy\Libraries\Cookie('SID'))->setValue($token)->setMaxAge(60 * 60 * 24)->setSameSiteRestriction('Strict')->save();
            $this->session_id = \Loggfy\Libraries\Cookie::get('SID');
        }
        return true;
    }

    public function add_to_queue($event = '', $params, $details = [])
    {
        $data = [
            'created_at' => date('Y-m-d H:i:s'),
            'session_id' => $this->session_id,
            'logable_type' => $event,
            'logable_id' => isset($params['id']) ? $params['id'] : 0,
            'context' => json_encode([
                'details' => $details,
                'user_agent' => $_SERVER['HTTP_USER_AGENT'],
                'referrer' => $_SERVER['HTTP_REFERER'],
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'language' => null,
                'country' => null,
                'device' => null,

            ])
        ];
        $this->post($data);
    }

    public function post($data)
    {
        global $table_prefix, $wpdb;

        $tblname = 'loggfy_logs';
        $wp_track_table = "{$table_prefix}{$tblname}";
        $wpdb->insert($wp_track_table, $data);
    }

    public function product_data($product)
    {
        $product_id = method_exists($product, 'get_id') ? $product->get_id() : $product->id;
        $data = [
            'id' => $product_id,
            'name' => $product->get_title(),
            'price' => $product->get_price(),
            'url' => get_permalink($product_id),
            'image_url' => null,
            'sku' => $product->get_sku()
        ];
        return $data;
    }

    public function loggfy_save_order($order_id)
    {
        $this->add_to_queue('checkout', ['id' => $order_id]);
    }

    public function loggfy_client_identify()
    {
        if (!is_admin() && is_user_logged_in() && !($this->session_get($this->get_identify_cookie_name()))) {
            $user = wp_get_current_user();
            $customer = array('customer_id' => $user->ID, 'email' => $user->user_email, 'name' => $user->display_name);
            $this->add_to_queue('identify', $customer);
            $this->session_set($this->get_identify_cookie_name(), 'true');
        }
    }

    public function session_set($k, $v)
    {
        if (!is_object($this->woo->session)) {
            @setcookie($k, $v, time() + 43200, COOKIEPATH, COOKIE_DOMAIN);
            $_COOKIE[$k] = $v;
            return true;
        }
        return $this->woo->session->set($k, $v);
    }

    private function get_identify_cookie_name()
    {
        return 'loggfy_id' . COOKIEHASH;
    }

    public function session_get($k)
    {
        if (!is_object($this->woo->session)) {
            return isset($_COOKIE[$k]) ? $_COOKIE[$k] : false;
        }
        return $this->woo->session->get($k);
    }

}
