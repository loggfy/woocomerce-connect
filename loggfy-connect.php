<?php
/*
 * Plugin Name: Loggfy
 * Plugin URI: https://www.loggfy.com
 * Description: 
 * Author: Loggfy.com
 * Version: 0.1
 * Author URI: https://loggfy.com
 * License: MIT
 * Text Domain: loggfy
 */

defined( 'ABSPATH' ) or die( 'Hey, what are you doing here? You silly human!' );

 
// Require once the Composer Autoload
if ( file_exists( dirname( __FILE__ ) . '/vendor/autoload.php' ) ) {
	require_once dirname( __FILE__ ) . '/vendor/autoload.php';
} 

/**
 * The code that runs during plugin activation
 */
function activate_loggfy_connect() {
    Loggfy\Base\Activate::activate();
}
register_activation_hook( __FILE__, 'activate_loggfy_connect' );

/**
 * The code that runs during plugin deactivation
 */
function deactivate_loggfy_connect() {
    Loggfy\Base\Deactivate::deactivate();
}
register_deactivation_hook( __FILE__, 'deactivate_loggfy_connect' );
 
if ( class_exists( 'Loggfy\Loggfy' ) ) {
    Loggfy\Loggfy::register_services();
		require('Loggfy_Bootstrap.php');
		new Loggfy_Bootstrap();
}