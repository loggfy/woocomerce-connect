<?php

namespace Loggfy\Services;




use Loggfy\Libraries\Carbon;

class Loggfy_Service extends Loggfy_BaseService
{

    function __construct()
    {
        parent::__construct();
    }

    public function dashboard_report($start_date, $end_date)
    {
        $start_day = Carbon::createFromFormat('Y-m-d', $start_date);
        $end_day = Carbon::createFromFormat('Y-m-d', $end_date);

        $error = null;
        $reports = [];
        $reports['data'] = [];
        $diff = $end_day->diffInDays($start_day);

        if ($diff > 1 && $diff < 16) {
            $i = 0;
            while ($i <= $diff) {
                $sum = $this->wpdb->get_var("SELECT  COUNT(DISTINCT session_id) FROM " . $this->wp_track_table . " WHERE DATE(created_at) = '" . $start_day->copy()->format('Y-m-d') . "';");
                $reports['data'][] = ['date' => $start_day->copy()->format('d M Y'), 'count' => $sum];
                $start_day->addDay();
                $i++;
            }
        } else {
            $reports['error'] = "Max length of report can be 15 days";
        }
        return $reports;
    }


    public function getSingleSession($session_id)
    {
        $sessions = $this->wpdb->get_results("SELECT * FROM " . $this->wp_track_table . " WHERE session_id = '" . $session_id . "'  order by created_at desc ;");
        return $sessions;
    }


    public function user_explorer_report($start_date, $end_date)
    {
        $start_day = Carbon::createFromFormat('Y-m-d', $start_date);
        $end_day = Carbon::createFromFormat('Y-m-d', $end_date);

        $error = null;
        $reports = [];
        $reports['data'] = [];
        $diff = $end_day->diffInDays($start_day);

        if ($diff > 1 && $diff < 16) {
            $i = 0;
            while ($i <= $diff) {
                $sum = $this->wpdb->get_var("SELECT count(distinct(session_id)) FROM " . $this->wp_track_table . " WHERE DATE(created_at) = '" . $start_day->copy()->format('Y-m-d') . "';");
                $reports['data'][] = ['date' => $start_day->copy()->format('Y-m-d'), 'formatted_date' => $start_day->copy()->format('d M Y'), 'count' => $sum];
                $start_day->addDay();
                $i++;
            }
        } else {
            $reports['error'] = "Max length of report can be 15 days";
        }
        return $reports;
    }

    public function getDailyUniqueUserLog($day)
    {
        $sql = "SELECT *  FROM " . $this->wp_track_table . " WHERE id IN (SELECT MAX(id) FROM " . $this->wp_track_table . " GROUP BY session_id)  AND DATE(created_at) = '" . $day . "' and session_id IS NOT NULL;";
        $rs = $this->wpdb->get_results($sql);
        return $rs;
    }

    public function daily_order_report($start_date, $end_date)
    {

        $start_day = Carbon::createFromFormat('Y-m-d', $start_date);
        $end_day = Carbon::createFromFormat('Y-m-d', $end_date);

        $error = null;
        $reports = [];
        $reports['data'] = [];
        $diff = $end_day->diffInDays($start_day);

        if ($diff > 1 && $diff < 16) {
            $i = 0;
            while ($i <= $diff) {
                $sum = $this->wpdb->get_var("SELECT count(*) FROM " . $this->wp_track_table . " WHERE logable_type = 'checkout' AND DATE(created_at) = '" . $start_day->copy()->format('Y-m-d') . "';");
                $reports['data'][] = ['date' => $start_day->copy()->format('d M Y'), 'count' => $sum];
                $start_day->addDay();
                $i++;
            }
        } else {
            $reports['error'] = "Max length of report can be 15 days";
        }
        return $reports;
    }
}