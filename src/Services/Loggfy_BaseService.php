<?php

namespace Loggfy\Services;

class Loggfy_BaseService
{
	protected $wp_track_table;
	protected $wpdb;
	
	public function __construct()
	{
		global $table_prefix, $wpdb;

		$tblname = 'loggfy_logs';
		$this->wp_track_table = $table_prefix . "$tblname";		
		$this->wpdb = $wpdb;
	}
	
	public function getDate($days = 0)
	{
		$date = date_create(date('Y-m-d'));
		if ($days > 0){
			date_sub($date, date_interval_create_from_date_string("$days days"));			
		}
		return date_format($date,"Y-m-d");		
	}
	
	public function getProduct($product_id = 0)
	{
		if (function_exists('wc_get_product')) {
				return wc_get_product($product_id);
		} else {
				return get_product($product_id);
		}		
	}
	
 
	
}