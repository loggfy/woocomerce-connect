<?php
/**
 * @package  Loggfy
 */
namespace Loggfy\Base;

class Deactivate
{
	public static function deactivate() {
		flush_rewrite_rules();
	}
}