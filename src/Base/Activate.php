<?php
/**
 * @package  Loggfy
 */

namespace Loggfy\Base;

class Activate
{
    public static function activate()
    {
        self::create_plugin_database_table();
        flush_rewrite_rules();
    }

    private static function create_plugin_database_table()
    {
        global $table_prefix;

        $tblname = 'loggfy_logs';
        $wp_track_table = "{$table_prefix}{$tblname}";

        $sql = "DROP TABLE IF EXISTS `{$wp_track_table}`; CREATE TABLE `{$wp_track_table}` (
						`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
						`session_id` varchar(55) COLLATE utf8mb4_unicode_ci,
						`logable_type` text COLLATE utf8mb4_unicode_ci,
						`logable_id` int(10) unsigned NULL ,
						`context` text COLLATE utf8mb4_unicode_ci,
						`created_at` timestamp NULL DEFAULT NULL,
						PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
        require_once(ABSPATH . '/wp-admin/includes/upgrade.php');
        dbDelta($sql);

    }

}