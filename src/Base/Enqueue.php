<?php 
/**
 * @package  Loggfy
 */
namespace Loggfy\Base;

use Loggfy\Base\BaseController;

/**
* 
*/
class Enqueue extends BaseController
{
	public function register() {
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue' ) );
	}
	
	function enqueue() {
		// enqueue all our scripts
		wp_enqueue_script( 'media-upload' );
		wp_enqueue_media();

		wp_enqueue_style( 'loggfy_bootstrap_css', $this->plugin_url . 'assets/css/bootstrap.min.css' );
		wp_enqueue_style( 'loggfy_pluginstyle', $this->plugin_url . 'assets/css/styles.css' );
 
		wp_enqueue_script( 'loggfy_bootstrap_js', $this->plugin_url . 'assets/js/bootstrap.bundle.js' );		
		wp_enqueue_script( 'loggfy_vendorscripts', $this->plugin_url . 'assets/js/scripts.js' );
		wp_enqueue_script( 'loggfy_chartscripts', $this->plugin_url . 'assets/js/Chart.min.js' );
	 	
	}
}