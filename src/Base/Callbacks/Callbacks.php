<?php
/**
 * @package  Loggfy
 */

namespace Loggfy\Base\Callbacks;

use Loggfy\Base\BaseController as BaseController;
use Loggfy\Libraries\UserAgent;
use Loggfy\Services\Loggfy_Service as Service; 

class Callbacks extends BaseController
{
    public function adminDashboard()
    {
        $service = new Service();
        $params = ['start_date', 'end_date'];
        $post = $_POST;

        $params['start_date'] = (isset($post['start_date'])) ? $post['start_date'] : $service->getDate(15);
        $params['end_date'] = (isset($post['end_date'])) ? $post['end_date'] : $service->getDate();

        $reports = $service->dashboard_report($params['start_date'], $params['end_date']);

        return require_once("$this->plugin_path/templates/dashboard.php");
    }

    public function loggfy_user_explorer()
    {
        $service = new Service();
				$get = $_GET;
				if(isset($get['day'])){
					$this->loggfy_user_explorer_day();
				}else{
					$params = ['start_date', 'end_date'];
					$post = $_POST;

					$params['start_date'] = (isset($post['start_date'])) ? $post['start_date'] : $service->getDate(15);
					$params['end_date'] = (isset($post['end_date'])) ? $post['end_date'] : $service->getDate();
					$reports = $service->user_explorer_report($params['start_date'], $params['end_date']);
					
	        return require_once("$this->plugin_path/templates/user_explorer.php");					
				}
    }

		public function loggfy_user_explorer_day()
		{
			$service = new Service();
			$get = $_GET;
			$day = (isset($get['day'])) ? $get['day'] : $service->getDate();
 			$rs = $service->getDailyUniqueUserLog($day);
			  
			return require_once("$this->plugin_path/templates/user_explorer_day.php");
		}
	
    public function adminOrders()
    {
        $service = new Service();
        $params = ['start_date', 'end_date'];
        $post = $_POST;

        $params['start_date'] = (isset($post['start_date'])) ? $post['start_date'] : $service->getDate(15);
        $params['end_date'] = (isset($post['end_date'])) ? $post['end_date'] : $service->getDate();

        $reports = $service->daily_order_report($params['start_date'], $params['end_date']);

        return require_once("$this->plugin_path/templates/dailyorders.php");
    }

    public function adminSessionView()
    {
        $userAgent = new UserAgent();
        $service = new Service();
				$browser = [];
				$get = $_GET;
				$session_id = (isset($get['session_id'])) ? $get['session_id'] : null;
			
        $sessions = $service->getSingleSession($session_id);
			
				if (isset($sessions[0])){
					$session = $sessions[0];

					$context = json_decode($session->context, true);
					$agent = $context['user_agent'];
					$browser = $userAgent->getBrowser($agent);
				}
        return require_once("$this->plugin_path/templates/session_view.php");
    }
}