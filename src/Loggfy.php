<?php
/**
 * @package LoggfyConnect
 */
namespace Loggfy;

use Loggfy\Controllers\Loggfy_SessionController;
use Loggfy\Controllers\Loggfy_DashboardController;

final class Loggfy{

    	/**
	 * Store all the classes inside an array
	 * @return array Full list of classes
	 */
	public static function get_services() 
	{
		return [ 
			Controllers\Loggfy_DashboardController::class,
			Controllers\Loggfy_UserExplorerController::class,
			Controllers\Loggfy_OrderController::class,
			Controllers\Loggfy_SessionController::class,
			Base\Enqueue::class,
		];
	}

	/**
	 * Loop through the classes, initialize them, 
	 * and call the register() method if it exists
	 * @return
	 */
	public static function register_services() 
	{
		foreach ( self::get_services() as $class ) {
			$service = self::instantiate( $class );
			if ( method_exists( $service, 'register' ) ) {
				$service->register();
			}
		}
	}

	/**
	 * Initialize the class
	 * @param  class $class    class from the services array
	 * @return class instance  new instance of the class
	 */
	private static function instantiate( $class )
	{
		$service = new $class();
		return $service;
	}
}