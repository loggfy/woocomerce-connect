<?php

namespace Loggfy\Controllers;

use Loggfy\Base\SettingsApi;
use Loggfy\Base\BaseController;
use Loggfy\Base\Callbacks\Callbacks;

/**
 *
 */
class Loggfy_OrderController extends BaseController
{
    public $callbacks;
    public $subpages = array();

    public function register()
    {
        $this->settings = new SettingsApi();
        $this->callbacks = new Callbacks();
        $this->setSubpages();
        $this->settings->addSubPages($this->subpages)->register();
    }

    public function setSubpages()
    {
        $this->subpages = array(
            array(
                'parent_slug' => 'loggfy_connect',
                'page_title' => 'Order Comparison',
                'menu_title' => 'Order Comparison',
                'capability' => 'manage_options',
                'menu_slug' => 'loggfy_orders',
                'callback' => array($this->callbacks, 'adminOrders'),
                'show_at_menu' => true
            )
        );
    }
}