<?php
namespace Loggfy\Controllers;

use Loggfy\Base\SettingsApi;
use Loggfy\Base\BaseController;
use Loggfy\Base\Callbacks\Callbacks;
 
class Loggfy_DashboardController extends BaseController
{
	public $settings;
	public $callbacks;
	public $callbacks_mngr;
	public $pages = array();
	public $subpages = array();

	public function register()
	{
			$this->settings = new SettingsApi();
  		$this->callbacks = new Callbacks();
		  $this->setPages();
			$this->settings->addPages( $this->pages )->withSubPage( 'Dashboard' )->register();
	}

	public function setPages()
	{
		$this->pages = array(
 			array(
				'page_title' => 'Loggfy',
				'menu_title' => 'Loggfy',
				'capability' => 'manage_options',
				'menu_slug' => 'loggfy_connect',
				'callback' => array( $this->callbacks, 'adminDashboard' ),
				'icon_url' => 'dashicons-chart-line',
				'position' => 10,
								'show_at_menu' => true
			)
		);
	} 
 
}