<?php

namespace Loggfy\Controllers;

use Loggfy\Base\SettingsApi;
use Loggfy\Base\BaseController;
use Loggfy\Base\Callbacks\Callbacks;

/**
 *
 */
class Loggfy_SessionController extends BaseController
{
    public $callbacks;
    public $subpages = array();

    public function register()
    {
        $this->settings = new SettingsApi();
        $this->callbacks = new Callbacks();
        $this->setSubpages();
        $this->settings->addSubPages($this->subpages)->register();
    }

    public function setSubpages()
    {
        $this->subpages = array(
            array(
                'page_title' => 'Session View',
                'menu_title' => 'Session View',
                'capability' => 'manage_options',
                'parent_slug' => 'loggfy_connect',
                'menu_slug' => 'loggfy_sessionview',
                'callback' => array($this->callbacks, 'adminSessionView'),
                'show_at_menu' => false
            )
        );
    }
}