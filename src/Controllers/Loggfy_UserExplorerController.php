<?php
namespace Loggfy\Controllers;

use Loggfy\Base\SettingsApi;
use Loggfy\Base\BaseController;
use Loggfy\Base\Callbacks\Callbacks;


class Loggfy_UserExplorerController extends BaseController
{
	public $callbacks;
	public $subpages = array();
	public function register()
	{
		$this->settings = new SettingsApi();
		$this->callbacks = new Callbacks();
		$this->setSubpages();
		$this->settings->addSubPages( $this->subpages )->register();
	}
	public function setSubpages()
	{
		$this->subpages = array(
			array(
				'parent_slug' => 'loggfy_connect',
				'page_title' => 'User Explorer',
				'menu_title' => 'User Explorer',
				'capability' => 'manage_options', 
				'menu_slug' => 'loggfy_user_explorer', 
				'callback' => array( $this->callbacks, 'loggfy_user_explorer' ),
                'show_at_menu' => true
			)		
			 
		);
	}
}