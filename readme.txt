=== Plugin Name ===
Contributors: loggfy
Donate link: http://www.loggfy.com
Author URI: http://www.loggfy.org/
Plugin URI: http://www.loggfy.org/
Tags: log analyze, trends, segment, woocommerce, wp e-commerce, ecommerce, management, reporting, analysis, sales, customers, graphs, charts, drill down
Requires at least: 4.7
Tested up to: 4.9
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Loggfy is a Woocomerce analytics and log management tool.

== Description ==

Loggfy is designed to manage e-commerce analyzes and segments the clients to observe new opportunities in revenue. Plugin provides detailed log and sale statistics. We try to create visit funnel for each client. We will
develop plugin regularly and add new features. Our aim is to create the best solution for woocomerce to analyze the web store.

== Support ==
If you need any help with Loggfy, we will be happy to help. Write your queries in the support section, or mail us at support@loggfy.com. Do not hesitate to remomend any new feature.

= Minimum Requirements =

* PHP version 5.4 or greater (PHP 7.2 or greater is recommended)
* MySQL version 5.0 or greater (MySQL 5.6 or greater is recommended)

= Automatic installation =

When you activate the plugin, it creates a database table which will hold the all the data. After activate the plugin, 
from Woocomerce settings page, click the Integrations tab.  Set status of the Loggfy to Active. Plugin will start to log and analyze the shop immadiately.

= Manual installation =

Upload all the files inside plugin directory. Later activate the plugin. After activate the plugin, 
from Woocomerce settings page, click the Integrations tab.  Set status of the Loggfy to Active. Plugin will start to log and analyze the shop immadiately.

== Frequently Asked Questions ==

= Where will data will be stored? =

All data and reportsed are in your own server. Because, you are holding all data in your database. But in feature we plan to release a custom plan for it. But always you will run all analuze toll in your own server.

= Is Loggfy free? =

Yes, it's free to use with in our own server.

= Which version of Woocomerce required =

We have used 3.4 while developing the Loggfy.


== Changelog ==

= 0.1 =
* First beta version fro public test.