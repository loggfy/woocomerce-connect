<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 p-3">
 						<h4>
							Unique Users at <?php echo $day ?> 
					</h4>


            <div class="table-responsive">
                <table class="table ">
                    <thead>
                    <tr>
                        <th scope="col">Client Id</th>
                        <th scope="col">View Type</th>
                        <th scope="col">Refering Page</th>
                        <th scope="col">Visit Time</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($rs as $r): ?>
                        <tr>
                            <td><?php echo $r->session_id ?></td>
                            <td><?php
                                $logable_type = $r->logable_type;
                                $logable_id = $r->logable_id;
                                echo "<strong>" . $logable_type . ": </strong> ";
                                if ($logable_type === 'product_view') {
                                    $product = $service->getProduct($logable_id);
                                    echo($product->get_name());
                                }

                                if ($logable_type === 'category_view') {
                                    $context = json_decode($r->context, true);
                                    if (isset($context['details'])) {
                                        $details = $context['details'];

                                        echo($details['name']);
                                    }

                                }


                                ?></td>
                            <td><?php
                                $context = json_decode($r->context, true);
                                echo $context['referrer'];
                                ?></td>
                            <td><?php
                                echo (date('d M Y H:i', strtotime($r->created_at))); ?></td>
                            <td>
                                <a class="btn btn-primary btn-sm"
                                   href="/wp-admin/admin.php?page=loggfy_sessionview&session_id=<?php echo $r->session_id ?>">View
                                    Session Log</a>
                            </td>
                        </tr>
                    <?php endforeach ?>

                    </tbody>
                </table>
            </div>
					</div>
			</div>
</div>
