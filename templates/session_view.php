<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 p-3">


            <h3>
                Session Report
            </h3>

						<?php
							if(count($sessions) < 1){ ?>
								<div class="alert alert-warning">
									There is no session log!
					</div>
							
					<?php		}else{	?>

            <div class="row" style="display:none">
                <div class="col-md-3">
                    Visitor Browser
                </div>
                <div class="col-md-3">
                    <?php echo (isset($browser['name'])) ? $browser['name'] : '--' ?>
                </div>

                <div class="col-md-3">
                    Operating System
                </div>
                <div class="col-md-3">
                    <?php echo $browser['platform'] ?>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table ">
                    <thead>
                    <tr>
                        <th scope="col">View Type</th>
                        <th scope="col">Refering Page</th>

                        <th scope="col">Visit Time</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($sessions as $r): ?>
                        <tr>
                            <td><?php
                                $logable_type = $r->logable_type;
                                $logable_id = $r->logable_id;
                                echo "<strong>" . $logable_type . ": </strong> ";


                                if (in_array($logable_type, ['add_to_cart', 'remove_from_cart', 'product_view'])) {
                                    $product = $service->getProduct($logable_id);
                                    echo $product->get_name();
                                }

                                if ($logable_type === 'category_view') {
                                    $context = json_decode($r->context, true);
                                    if (isset($context['details'])) {
                                        $details = $context['details'];

                                        echo($details['name']);
                                    }
                                }
                                ?></td>

                            <td><?php
                                $context = json_decode($r->context, true);
                                echo $context['referrer'];
                                ?></td>
                            <td><?php
                                echo(date('d M Y H:i', strtotime($r->created_at))); ?></td>
                            <td>

                            </td>
                        </tr>
                    <?php endforeach ?>

                    </tbody>
                </table>
            </div>
<?php } ?>
        </div>
    </div>
</div>