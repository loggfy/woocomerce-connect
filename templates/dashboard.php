<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 p-3">
 						<h4>
							Audience Overview
					</h4>
            <canvas id="canvas" height="100"></canvas>

            <?php

            global $table_prefix, $wpdb;

            $tblname = 'loggfy_logs';
            $wp_track_table = $table_prefix . "$tblname";

            $sql = 'SELECT *  FROM ' . $wp_track_table . ' WHERE id IN (SELECT MAX(id) FROM '. $wp_track_table .' GROUP BY session_id) and session_id IS NOT NULL order by created_at desc limit 0,20 ';
            $rs = $wpdb->get_results($sql);

            ?>

            <div class="table-responsive">
                <table class="table ">
                    <thead>
                    <tr>
                        <th scope="col">Client Id</th>
                        <th scope="col">View Type</th>
                        <th scope="col">Refering Page</th>
                        <th scope="col">Visit Time</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($rs as $r): ?>
                        <tr>
                            <td><?php echo ($r->session_id) ?></td>
                            <td><?php
                                $logable_type = $r->logable_type;
                                $logable_id = $r->logable_id;
                                echo "<strong>" . $logable_type . ": </strong> ";
                                if ($logable_type === 'product_view') {
                                    $product = $service->getProduct($logable_id);
                                    echo($product->get_name());
                                }

                                if ($logable_type === 'category_view') {
                                    $context = json_decode($r->context, true);
                                    if (isset($context['details'])) {
                                        $details = $context['details'];

                                        echo($details['name']);
                                    }

                                }


                                ?></td>
                            <td><?php
                                $context = json_decode($r->context, true);
                                echo $context['referrer'];
                                ?></td>
                            <td><?php
                                echo (date('d M Y H:i', strtotime($r->created_at))); ?></td>
                            <td>
                                <a class="btn btn-primary btn-sm"
                                   href="/wp-admin/admin.php?page=loggfy_sessionview&session_id=<?php echo $r->session_id ?>">View
                                    Session Log</a>
                            </td>
                        </tr>
                    <?php endforeach ?>

                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

<?php
    $unsorted_reports = $reports['data'];
?>
<script>
    var config = {
        type: 'bar',
        options: {
            responsive: true,
						showLines: false,
						spanGaps: false,
            title: {
                display: true,
                text: 'Daily Unique Visit Comparison'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            } 
        }
    };

    window.onload = function () {
        var ctx = document.getElementById('canvas').getContext('2d');
        window.myLine = new Chart(ctx, config);

        var newDataset = {
            label: 'user count',
            backgroundColor: '#0169D9',
            borderColor: '#24456A',
            data: []
        };
				config.data.datasets.push(newDataset);

        <?php foreach($unsorted_reports as $report){ ?>
        config.data.labels.push('<?php echo $report['date'] ?>');
        config.data.datasets.forEach(function (dataset) {
            dataset.data.push(<?php echo $report['count'] ?>);
        });
        window.myLine.update();
        <?php } ?>
    };
</script>