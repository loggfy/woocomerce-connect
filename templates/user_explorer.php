<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 p-3">

			<h3>
				User Explorer
			</h3>
			
	<div class="row">
		<div class="col-md-7"></div>
					<div class="col-md-5">
			<form action="?page=loggfy_user_explorer" method="post">
				<div class="form-row">
					<div class="col-sm-4">
						<input type="date" class="form-control" name="start_date" value="<?php echo $params['start_date'] ?>" />
					</div>
					<div class="col-sm-4">
						<input type="date" class="form-control" name="end_date" value="<?php echo $params['end_date'] ?>" />
					</div>
					<div class="col-sm-2">
						<button type="submit" class="btn btn-primary btn-md">Get Report</button>
					</div>
				</div>
			</form>
		</div>
			</div>
		 
			<canvas id="canvas" height="100"></canvas>

			<?php if (isset($reports['error'])){ ?>
				<div class="alert alert-danger">
					<?php echo $reports['error'] ?>
			</div>
			<?php } ?>
			<?php if (count($reports['data']) > 0){ ?>
				<div class="table-responsive">
			<table class="table table-hover">
			<thead class="thead-dark">
					<tr>
						<th scope="col">Date</th>
						<th scope="col">Unique Session Count</th>
						<th scope="col">&nbsp; </th>
					</tr>
				</thead>				
				<tbody>
					<?php 
					$unsorted_reports = $reports['data'];
					usort($reports['data'], function($a, $b) {
  												  return $a['date'] < $b['date'];
												});
				 
					foreach($reports['data'] as $report){ ?>
					<tr>
						<td style="width: 70%">
							<?php echo $report['formatted_date'] ?>
						</td>
						<td>
							<?php echo $report['count'] ?>
						</td>
						<td>
						<a class="btn btn-primary pull-right" href="?page=loggfy_user_explorer&day=<?php  echo $report['date'] ?>">Show Details</a>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
			</div>
			<?php } ?>
		</div>
	</div>
</div>
 
<script>
	var config = {
		type: 'bar',
		options: {
			responsive: true,
			title: {
				display: true,
				text: 'Daily Unique Sessions'
			},
			tooltips: {
				mode: 'index',
				intersect: false,
			},
			hover: {
				mode: 'nearest',
				intersect: true
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Count of Sessions'
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Sessions'
					}
				}]
			}
		}
	};

	window.onload = function() {
		var ctx = document.getElementById('canvas').getContext('2d');
		window.myLine = new Chart(ctx, config);

		var newDataset = {
			label: 'Daily Unique Sessions',
			backgroundColor: '#0169D9',
			borderColor: '#F0FFFF',
			data: []
		};
		config.data.datasets.push(newDataset);

		<?php foreach($unsorted_reports as $report){ ?>
		config.data.labels.push('<?php echo $report['date'] ?>');
		config.data.datasets.forEach(function(dataset) {
			dataset.data.push(<?php echo $report['count'] ?>);
		});
		window.myLine.update();
		<?php } ?>

	};
</script>