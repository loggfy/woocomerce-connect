<?php
 

class Loggfy_Bootstrap {
    /**
    * Construct the plugin.
    */
	public function __construct() {
		add_action( 'plugins_loaded', array( $this, 'init' ) );
	}
    /**
    * Initialize the plugin.
    */
    public function init() {
        include_once(__DIR__ .'/Loggfy_Integration.php');
        add_filter( 'woocommerce_integrations', array( $this, 'loggfy_add_integration' ) );
    }

    /**
     * Add a new integration to WooCommerce.
     */
    public function loggfy_add_integration( $integrations ) {
        $integrations[] = Loggfy_Integration::class;
        return $integrations;
    }
}